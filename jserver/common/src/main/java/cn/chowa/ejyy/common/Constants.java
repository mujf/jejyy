package cn.chowa.ejyy.common;

public interface Constants {


    interface authenticted {
        // 业主认证来源
        int BY_SELF = 1;
        int BY_PROPERTY_COMPANY = 2;
        int BY_FAMILY = 3;
    }

    interface building {
        int HOUSE = 1;
        int CARPORT = 2;
        int WAREHOUSE = 3;
        int MERCHANT = 4;
        int GARAGE = 5;
    }

    interface car {
        int BLUE_PLATE_CAR = 1;
        int YELLOW_PLATE_CAR = 2;
    }

    interface code {
        // http 状态码
        int SUCCESS = 200;
        int FAILE = -200;
        // 系统未初始化
        int SYSTEMT_NOT_INIT = -66;
        // 系统已经初始化了
        int SYSTEMT_ALREADY_INIT = -78;
        // 账号不存在
        int ACCOUNT_NOT_EXIST = -100;
        // 账号存在
        int ACCOUNT_EXIST = -101;
        // 验证码错误
        int CAPTCHA_ERROR = -102;
        // 未登录
        int NOT_LOGIN = -110;
        // 密码错误
        int PWD_ERROR = -111;
        // 参数错误
        int PARAMS_ERROR = -112;
        // 微信授权登录失败
        int WEHCAT_MP_LOGIN_ERROR = -113;
        // 微信回去手机号失败
        int WEHCAT_MP_GET_PHONE_ERROR = -114;
        // 未查询到关联住宅信息
        int NOT_FOUND_BINDING_BUILDING = -115;
        // 二维码非法
        int QRCODE_ILLEGAL = -116;
        // 二维码过期
        int QRCODE_EXPIRED = -117;
        // 已经加入了家园体验
        int HAS_JOIN_EXPERIENCE = -118;
        // 社区不存在
        int COMMUNITY_ID_NOT_EXIST = -119;
        // 数据更新失败
        int DATA_MODEL_UPDATE_FAIL = -120;
        // 需要绑定的车牌数量有限
        int EXCEED_ALLOW_BINDING_CAR_NUMBER = -121;
        // 车辆已绑定过
        int CAR_NUMBER_ALEADY_EXIST = -122;
        // 解绑家人情况非物业认证业主
        int UNBINDING_FAMILY_ILLEGAL = -123;
        // 账户被冻结
        int ACCOUNT_HAS_BEEN_FREEZE = -124;
        // 提交超过限制
        int SUBMIT_EXCEED_LIMIT = -125;
        // 用户未绑定手机号码
        int USER_NOT_BINDING_PHONE = -126;
        // 身份证号非法 没用到
        int IDCARD_ILLEGAL = -127;
        // 远程开门失败
        int REMOTE_OPEN_DOOR_FAIL = -128;
        // 维修已经评论过
        int REPAIR_RATE_EXIST = -129;
        // 查询非法 对应查询 detail
        int QUERY_ILLEFAL = -130;
        // 维修催促 工单已完成
        int URGE_FAIL_ON_FINISH_REPAIR = -131;
        // 提交频次超限
        int EXCED_ALLOW_FREQUENCY = -132;
        // 支付创建订单存在已支付或已创建的订单
        int PAYMENT_CREATE_ORDER_FAIL = -133;
        // 创建支付订单的建筑id非法，也就不是自己的
        int PAYMENT_BUILDING_ILLEGAL = -134;
        // 取消订单失败
        int PAYMENT_CANCEL_ORDER_FAIL = -135;
        // 状态错误
        int STATUS_ERROR = -136;
        // 装修报备已存在
        int FITMENT_CREATE_FAIL = -137;
        // 问卷已答
        int QUESTIONNAIRE_HAS_ANSWERED = -138;
        // 微信web登录授权码错误
        int WECHAT_STATE_ILLEGAL = -139;
        // 微信web登录失败
        int WEHCAT_WEB_LOGIN_FAIL = -140;
        // 公司信息已经存在
        int COMPANY_INFO_EXIST = -141;
        // 每个人只能申请一家物业公司入驻
        int APPLY_COMPANY_REPEAT = -142;
        // 权限错误
        int ACCESS_DENY = -143;
        // 数据删除失败
        int DATA_MODEL_REMOVE_FAIL = -144;
        // 访客二维码错误
        int VISTOR_QRCODE_ERROR = -145;
        // 访客二维码过期
        int VISTOR_QRCODE_EXPIRED = -146;
        // 访客二维码使用了
        int VISTOR_QRCODE_USED = -147;
        // 导入模板粗错误
        int IMPORT_TEMPLATE_ERROR = -149;
        // 非法的物业用户
        int ILLEGAL_PROPERTY_COMPANY_USER = -151;
        // 为完善身份信息
        int USER_INFO_UNINTACT = -152;
        // 数据库字段重复
        int MODEL_FIELD_VALUE_EXIST = -153;
        // 不存在流程
        int WORKFLOW_NOT_EXIST = -154;
        // 工作流非法
        int WORKFLOW_ILLEGAL = -155;
        // 物品分类已经存在
        int MATERIAL_CATEGORY_EXIST = -156;
        // 物品重复了
        int MATERIAL_EXIST = -157;
        // 仓库名称存在
        int STOREHOUSE_EXIST = -158;
        // 供货商存在
        int MATERIAL_SUPPLIER_EXIST = -158;
        // 会议室存在
        int MEETING_ROOM_EXIST = -159;
        // 会议时间重复
        int MEETING_TIME_REPEAT = -160;
        // 任务类型存在
        int MISSION_CATEGORY_EXIST = -161;
        // 任务点存在
        int MISSION_POINT_EXIST = -162;
        // 任务路线存在
        int MISSION_LINE_EXIST = -163;
        // 未配置考勤
        int WORK_CHECK_NOT_EXIST = -164;
        // 门禁存在
        int ENTRANCE_NAME_EXIST = -165;
        // 梯控存在
        int ELEVATOR_NAME_EXIST = -166;
        // 灯控存在
        int LAMP_NAME_EXIST = -167;
        // 灯控线路存在
        int LAMP_LINE_NAME_EXIST = -168;
        // 中继器名称存在
        int REPEATER_NAME_EXIST = -169;
        // 仪表名称存在
        int METER_NAME_EXIST = -170;
        // 停车场名称存在
        int PARK_NAME_EXIST = -171;
        // 黑名单存在
        int PARK_BLACKLIST_EXIST = -172;
        // 预警中控名称存在
        int WARNING_NAME_EXIST = -173;
        // 门禁非法
        int IOT_ENTRANCE_ILLEGAL = -174;
        // 梯控非法
        int IOT_ELEVATOR_ILLEGAL = -175;
        // 灯控非法
        int IOT_LAMP_ILLEGAL = -176;
        // 中继器非法
        int IOT_REPEATER_ILLEGAL = -177;
        // 仪表非法
        int IOT_METER_ILLEGAL = -178;
        // 停车差非法
        int IOT_PARK_ILLEGAL = -179;
        // 预警中控非法
        int IOT_WARNING_ILLEGAL = -180;
        // 物联网设备秘钥错误
        int IOT_SECRET_ERROR = -181;
    }

    interface complain {

        int COMPLAIN = 1;
        int SUGGEST = 2;

        // 投诉类型
        int COMPLAIN_HEALTH = 1;
        int COMPLAIN_NOISE = 2;
        int COMPLAIN_SERVICE = 3;
        int COMPLAIN_BUILDING = 4;
        int COMPLAIN_FIRE_ACCESS = 5;
        int COMPLAIN_COMMUNITY_FACILITY = 6;
        int COMPLAIN_OTHER = 7;

        // 业主提交 客服调拨 指派人员处理 完成
        // 1 已提交，待客服响应
        // 2 分配xxx进行进行处理
        // 3 指派人员处理正在处理
        // 4 完成
        int SUBMIT_COMPLAIN_STEP = 1;
        int ALLOT_COMPLAIN_STEP = 2;
        int CONFIRM_COMPLAIN_STEP = 3;
        int FINISH_COMPLAIN_STEP = 4;

    }

    interface enter_access {
        int SELF_ACCESS_CODE = 1;
        int VISTOR_ACCESS_CODE = 2;
    }

    interface epidemic {
        int GREEN_TOUR_CODE = 1;
        int YELLOW_TOUR_CODE = 1;
        int RED_TOUR_CODE = 1;

    }

    interface feedback {
        int FEEDBACK_OF_FEATURE = 1;
        int FEEDBACK_OF_PROBLEM = 2;

    }

    interface fitment {
        int USER_SUBMIT_APPLY_STEP = 1;
        int PROPERTY_COMPANY_ALLOW_STEP = 2;
        int USER_FINISH_FITMENT_STEP = 3;
        int PROPERTY_COMPANY_CONFIRM_STEP = 4;
    }

    interface iot {

        // 通行类型
        int IOT_METHOD_QRCODE = 1;
        int IOT_METHOD_NFC = 2;
        int IOT_METHOD_ICCARD = 3;

        // 仪表类型
        int IOT_METER_WATER = 1;
        int IOT_METER_ELECTRICITY = 2;
        int IOT_METER_GAS = 3;

        // 梯控
// 凯帕斯
        int ENTRANCE_KAI_PA_SI = 1;

        // 中继器
        int REPEATER_XUAN_KUN = 1;
        int REPEATER_YOU_REN = 2;

        // 预警类型
        int WARNING_OF_WATER = 1;
        int WARNING_OF_FIRE = 2;
        int WARNING_OF_GAS = 3;

    }

    interface material {
        int MATERIAL_ORIGIN_INIT = 1;
        int MATERIAL_ORIGIN_BUY = 2;
        int MATERIAL_ORIGIN_TRANSFER = 3;
    }

    interface move_car {

        // 阻碍通行
        int MOVE_CAR_BECAUSE_OF_GO_THROUGH = 1;
        // 占用消防通道
        int MOVE_CAR_BECAUSE_OF_FIRE_ENGINE_ACCESS = 2;
        // 阻碍入口
        int MOVE_CAR_BECAUSE_OF_BLOCK_ENTRANCE = 3;
        // 影响施工
        int MOVE_CAR_BECAUSE_OF_EFFECT_WORK = 4;
        // 占用车位
        int MOVE_CAR_BECAUSE_OF_OCCUPY_PORT = 5;

    }

    interface notice {
        int PRPERTY_COMANDY_NOTICE = 1;
        int SYSTEM_NOTICE = 2;
    }

    interface operate_type {

        // 操作类型
        int OPEARTE_BY_SELF = 1;
        int OPEARTE_BY_FAMILY = 2;
        int OPEARTE_BY_COMPANY = 3;

    }

    interface pay {

        // 退款状态
        int REFUND_SUCCESS = 1;
        int REFUND_CHANGE = 2;
        int REFUND_REFUNDCLOSE = 3;

        // 支付状态常量
        String PAY_SUCCESS = "SUCCESS";
        String PAY_FAIL = "FAIL";

        // 查询状态
        int ORDER_CANCEL_STATUS = 1;
        int ORDER_SUCCESS_STATUS = 2;
        int ORDER_REFUNDING_STATUS = 3;
        int ORDER_REFUNDED_STATUS = 4;
        int ORDER_EXPIRED_STATUS = 5;
        int ORDER_NEED_PAY_STATUS = 6;

    }

    interface pet {

        int DOG = 1;

        int MALE = 1;
        int FEMALE = 0;

        // 注销原因
        int REMOVE_PET_BECAUSE_DIE = 1;
        int REMOVE_PET_BECAUSE_LOSE = 2;
        int REMOVE_PET_BECAUSE_GIVE = 3;
        int REMOVE_PET_BECAUSE_CONFISCATE = 4;

    }

    interface questionnaire {
        int SIGNLE_CHOICE = 1;
        int MULTIPLE_CHOICE = 2;
    }

    interface repair {

        int WATER_AND_HEATING = 1;
        int ELECTRICITY = 2;
        int DOOR_AND_WINDOW = 3;
        int PUBLIC_FACILITY = 4;

// 业主提交 客服调拨 维修上门 维修完成 评价
// 1 已提交，待客服响应
// 2 分配xxx进行维修
// 3 维修人员正在赶往处置
// 4 维修完成

        int SUBMIT_REPAIR_STEP = 1;
        int ALLOT_REPAIR_STEP = 2;
        int CONFIRM_REPAIR_STEP = 3;
        int FINISH_REPAIR_STEP = 4;

    }

    /**
     * 角色名
     */
    interface RoleName {
        String DJDX = "ROLE_DJDX";
        String HYSGL = "ROLE_HYSGL";
        String RLZY = "ROLE_RLZY";
        String XQTZ = "ROLE_XQTZ";
        String CWDA = "ROLE_CWDA";
        String ZXDJ = "ROLE_ZXDJ";
        String WXWF = "ROLE_WXWF";
        String TSJY = "ROLE_TSJY";
        String CLGL = "ROLE_CLGL";
        String XQNC = "ROLE_XQNC";
        String FKTX = "ROLE_FKTX";
        String WJDC = "ROLE_WJDC";
        String CWGL = "ROLE_CWGL";
        String HTGL = "ROLE_HTGL";
        String FCDA = "ROLE_FCDA";
        String YZDA = "ROLE_YZDA";
        String WLCC = "ROLE_WLCC";
        String YQFK = "ROLE_YQFK";
        String XJRW = "ROLE_XJRW";
        String XZTZ = "ROLE_XZTZ";
        String ZNMJ = "ROLE_ZNMJ";
        String ZNTK = "ROLE_ZNTK";
        String ZHZM = "ROLE_ZHZM";
        String NHGL = "ROLE_NHGL";
        String ZHTC = "ROLE_ZHTC";
        String ZHYJ = "ROLE_ZHYJ";
        String ZTGL = "ROLE_ZTGL";
        String ANYONE = "ROLE_ANYONE";
    }

    enum Role {
        /**
         * 0 任何人
         */
        ANYONE,
        /**
         * 1 党建党训
         */
        DJDX,
        /**
         * 2 会议室管理
         */
        HYSGL,
        /**
         * 3 人力资源
         */
        RLZY,
        /**
         * 4 小区通知
         */
        XQTZ,
        /**
         * 5 宠物档案
         */
        CWDA,
        /**
         * 6 装修登记
         */
        ZXDJ,
        /**
         * 7 维修维护
         */
        WXWF,
        /**
         * 8 投诉建议
         */
        TSJY,
        /**
         * 9 车辆管理
         */
        CLGL,
        /**
         * 10 小区挪车
         */
        XQNC,
        /**
         * 11 访客通行
         */
        FKTX,
        /**
         * 12 问卷调查
         */
        WJDC,
        /**
         * 13 财务管理
         */
        CWGL,
        /**
         * 合同管理
         */
        HTGL,
        /**
         * 15 房产档案
         */
        FCDA,
        /**
         * 16 业主档案
         */
        YZDA,
        /**
         * 17 物料仓储
         */
        WLCC,
        /**
         * 18 疫情防控
         */
        YQFK,
        /**
         * 19 巡检任务
         */
        XJRW,
        /**
         * 20 行政通知
         */
        XZTZ,
        /**
         * 21 智能门禁
         */
        ZNMJ,
        /**
         * 22 智能梯控
         */
        ZNTK,
        /**
         * 23 智慧照明
         */
        ZHZM,
        /**
         * 24 能耗管理
         */
        NHGL,
        /**
         * 25 智慧停车
         */
        ZHTC,
        /**
         * 26 智慧预警
         */
        ZHYJ,
        /**
         * 27 专题管理
         */
        ZTGL;

        @Override
        public String toString() {
            return name();
        }
    }

    interface schedule {

        String VIRUS_JOB = "VIRUS_JOB";
        String SESSION_JOB = "SESSION_JOB";
        String MOCK_JOB = "MOCK_JOB";

    }

    interface status {

// 各类数据表状态

        // 正常和冻结
        int NORMAL_STATUS = 1;
        int FREEZE_STATUS = 0;

        // 与非
        int TRUE = 1;
        int FALSE = 0;

        // 用户住宅
        int BINDING_BUILDING = 1;
        int UNBINDING_BUILDING = 0;

        // 车辆
        int BINDING_CAR = 1;
        int UNBINDING_CAR = 0;

        // 完善用户信息
        int INTACT_USER_INFO = 1;
        int INCOMPLETE_USER_INFO = 0;

        // 小区门禁
        int ACCESS_NFC_AVAILABLE = 1;
        int ACCESS_NFC_DISABLED = 0;

        // 小区二维码
        int ACCESS_QRCODE_AVAILABLE = 1;
        int ACCESS_QRCODE_DISABLED = 0;

        // 小区远程开门
        int ACCESS_REMOTE_DISABLED = 0;
        int ACCESS_REMOTE_AVAILABLE = 1;

        // 装修保证经
        int FIXMENT_PLEDGE_DISABLED = 0;
        int FIXMENT_PLEDGE_AVAILABLE = 1;

    }

    interface tpl {

        // 小程序的
// 报修 分配工单 确认工单 工单完成
        String MP_REPAIR_ALLOT_TPL = "";
        String MP_REPAIR_CONFIRM_TPL = "";
        String MP_REPAIR_FINISH_TPL = "";

        // 投诉 分配工单 确认工单 工单完成
        String MP_COMPLAIN_ALLOT_TPL = "";
        String MP_COMPLAIN_CONFRIM_TPL = "";
        String MP_COMPLAIN_FINISH_TPL = "";

        // 挪车
        String MP_MOVE_CAR_TPL = "";

        // 业主认证的
        String MP_OWER_APPROVE = "";

        // 公众号的
// 通知物业工单
        String OA_NOTICE_TO_PROPERTY_COMPANY_USER = "";

        // 访客通知
        String OA_NOTICE_TO_VISTOR = "";

        // 小区通知
// 停水 1
        String OA_NOTICE_COMMUNITY_USER_STOP_WATER = "STOP_WATER";
        // 停电 2
        String OA_NOTICE_COMMUNITY_USER_STOP_ELECTRICITY = "STOP_ELECTRICITY";

        // 会议通知
        String OA_MEETING_BROADCAST = "";

        // 流程审批通知
        String OA_NOTICE_WORKFLOW_APPROVER = "";
        // 流程审批结果
        String OA_NOTICE_WORKFLOW_RESULT = "";
        // 物业收费通知
        String OA_NOTICE_OWER_PROPERTY_FEE = "";
        // 物业费催缴
        String OA_NOTICE_URGE_OWER_PROPERTY_FEE = "";

// 短信推送

        // 物业收费通知
        String SMS_NOTICE_OWER_PROPERTY_FEE = "";
        // 物业费催缴
        String SMS_NOTICE_URGE_OWER_PROPERTY_FEE = "";

    }

    interface workflow {

        // 流程类型
// 请假
        int LEAVE_WORKFLOW = 1;
        // 报销
        int REFOUND_WORKFLOW = 2;
        // 采购
        int PURCHASE_WORKFLOW = 3;

        // 节点类型
// 发起人节点
        int WORKFLOW_NODE_INITIATE = 1;
        // 审批人节点
        int WORKFLOW_NODE_APPROVER = 2;
        // 抄送接送
        int WORKFLOW_NODE_NOTICE = 3;
        // 条件节点
        int WORKFLOW_NODE_CONDITION = 4;
        // 判断节点 路由节点
        int WORKFLOW_NODE_JUDGE = 5;

        // 运算关系
// 小于
        int OPT_LT = 1;
        // 大于
        int OPT_GT = 2;
        // 小于等于
        int OPT_LT_EQUAL = 3;
        // 等于
        int EQUAL = 4;
        // 大于等于
        int OPT_GT_EQUAL = 5;
        // 介于连个数质检
        int OPT_BETWEEN = 6;

        // 判断分类
        int CONDITION_DEPARMENT = 1;
        int CONDITION_NUMBER = 2;

    }
}
