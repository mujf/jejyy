package cn.chowa.ejyy.ower;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.PagedData;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.OwerQuery;
import cn.dev33.satoken.annotation.SaCheckRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName apply_list
 * @Description TODO
 * @Author ironman
 * @Date 11:56 2022/8/23
 */
@Slf4j
@RestController
@RequestMapping("/pc/ower")
public class apply_list {

    @Autowired
    private OwerQuery owerQuery;

    /**
     * 查询业主档案列表 - YZDA
     * 备注:  .select(ctx.model.raw('SQL_CALC_FOUND_ROWS ejyy_ower_apply.id')) 未处理
     */
    @SaCheckRole(Constants.RoleName.YZDA)
    @VerifyCommunity(true)
    @PostMapping("/apply_list")
    public PagedData<ObjData> getList(@RequestBody RequestData data) {
        long communityId = data.getCommunityId();
        boolean replied =  data.getBool("replied", false, "^0|1$");;
        boolean subscribed = data.getBool("subscribed", false, "^0|1$");
        boolean success = data.getBool("success", true, "^0|1$");
        //列表查询
        List<ObjData> temp = owerQuery.getOwerApplyList(communityId,replied,subscribed,success,data.getPageSize(),data.getPageNum());
        long total = owerQuery.getOwerApplyListCount(communityId,replied,subscribed,success);
        return PagedData.of(data.getPageNum(), data.getPageSize(),temp, total);
    }

}
