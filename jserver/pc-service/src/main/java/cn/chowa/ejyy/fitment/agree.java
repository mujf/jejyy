package cn.chowa.ejyy.fitment;

import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.dao.FitmentQuery;
import cn.chowa.ejyy.dao.FitmentRepository;
import cn.chowa.ejyy.model.entity.Fitment;
import cn.dev33.satoken.annotation.SaCheckRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static cn.chowa.ejyy.common.Constants.code.DATA_MODEL_UPDATE_FAIL;
import static cn.chowa.ejyy.common.Constants.fitment.PROPERTY_COMPANY_ALLOW_STEP;
import static cn.chowa.ejyy.common.Constants.fitment.USER_SUBMIT_APPLY_STEP;

@RestController("fitmentAgree")
@RequestMapping("/pc/fitment")
public class agree {

    @Autowired
    private FitmentRepository fitmentRepository;

    @Autowired
    private FitmentQuery fitmentQuery;

    /**
     * 装修登记-同意
     */
    @SaCheckRole(Constants.RoleName.ZXDJ)
    @VerifyCommunity(true)
    @PostMapping("/agree")
    public Map<?, ?> agree(@RequestBody RequestData data) {
        long id = data.getLong("id", true, "^\\d+$");
        Integer cash_deposit = data.getInt("cash_deposit", false, "^\\d+$");
        String return_name = data.getStr("return_name");
        String return_bank = data.getStr("return_bank");
        String return_bank_id = data.getStr("return_bank_id");
        long agreed_at = System.currentTimeMillis();

        Fitment fitment = fitmentRepository.findByIdAndCommunityIdAndStep(id, data.getCommunityId(), USER_SUBMIT_APPLY_STEP);
        if (fitment == null) {
            throw new CodeException(DATA_MODEL_UPDATE_FAIL, "同意装修申请失败");
        }

        fitment.setCash_deposit(cash_deposit);
        fitment.setReturn_name(return_name);
        fitment.setReturn_bank(return_bank);
        fitment.setReturn_bank_id(return_bank_id);
        fitment.setAgree_user_id(AuthUtil.getUid());
        fitment.setAgreed_at(agreed_at);
        fitment.setStep(PROPERTY_COMPANY_ALLOW_STEP);
        fitmentRepository.save(fitment);

        return Map.of(
                "agreed_at", agreed_at,
                "agreeUserInfo", Map.of(
                        "id", AuthUtil.getUid(),
                        "real_name", ""
                )
        );

    }

}
