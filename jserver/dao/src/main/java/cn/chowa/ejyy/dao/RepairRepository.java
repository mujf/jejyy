package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.Repair;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RepairRepository extends JpaRepository<Repair, Long> {

    List<Repair> findByCommunityIdAndCreatedAtBetween(long communityId, long start, long end);

}
